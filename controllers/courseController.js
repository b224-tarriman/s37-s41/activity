const Course = require('../models/Course')
const User = require('../models/Course')
const auth = require("../auth")
const bcrypt = require("bcrypt")
// const auth = require("../auth")
// Controller Functions
// Creating a new course

module.exports.addCourse = (reqBody) =>{
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        })

        return newCourse.save().then((course,err)=>{
            if(err){
                return false
            }else{
                return "Added by admin"
            }
        })
    
}
// Retrieving all courses
module.exports.getAllCourses = (data) =>{
    if(data.isAdmin){
        return Course.find({}).then(result =>{
            return result
        })
    }else{
        return false
    }
}
module.exports.getAllActive = () =>{
    return Course.find({isActive:true}).then(result=>{
        return result
    })
}
// Retrive specific course
module.exports.getCourse = (id) =>{
    
    return Course.findById(id.courseId)
    .then(result=> {return result})
}

module.exports.updateCourse = (reqParams,reqBody)=>{
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then((success,error)=>{
        console.log(success)
        if(error){
            return false
        }else{
            return true
        }
    })
       
    
}

module.exports.archiveCourse = async (reqParams,reqBody)=>{
    let updatedActive = {
        isActive: reqBody.isActive
    }
    try{
    await Course.findByIdAndUpdate(reqParams.courseId,updatedActive)
    return `Archived Course ${reqParams.courseId}`
    }catch(error){
        return false
    }
}