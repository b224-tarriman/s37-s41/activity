const express= require("express")
const userController = require("../controllers/userController")
const router = express.Router()
const auth = require("../auth")

router.post("/checkEmail",(req,res)=>{
    userController
    .checkEmailExists(req.body)
    .then(result =>res.send(result))
})

// Route for user registration
router.post("/register",(req,res)=>{
    userController.registerUser(req.body)
    .then(result=>res.send(result))
})

// Route for user log in
router.post("/login", (req,res)=>{
    userController.loginUser(req.body)
    .then(result => res.send(result))
})

// router.post("/details",(req,res)=>{
//     userController.getProfile(req.body)
//     .then(result=>res.send(result))
// })

// Retrieve user details
router.post("/details", auth.verify,(req, res) => {

    const userData = auth.decode(req.headers.authorization);
    console.log(userData)
    console.log(req.headers.authorization)
    
	userController.getProfile({id:userData.id}).then(resultFromController => res.send(resultFromController))
});

router.post("/enroll",auth.verify,(req,res)=>{
    
    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin){
        res.send("You can not enroll to a course, admin.")
    }else{
       
        let data ={
            courseId:req.body.courseId,
        }
        
        userController.enroll(data,{id:userData.id}).then(result=>res.send(result))
        
    }  
})

module.exports = router