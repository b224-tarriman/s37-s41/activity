const express= require("express")
const courseController = require("../controllers/courseController")
const router = express.Router()
const auth = require("../auth")

// Routes for creating a course
router.post("/addCourse", auth.verify, (req,res)=>{ 
    
    const userData = auth.decode(req.headers.authorization);
    console.log(userData)
    if(userData.isAdmin){
    courseController.addCourse(req.body)
    .then(result => res.send(result))
    }else{
        res.send({auth:"failed"})
    }
})

// Route for retrieving all the courses

router.get("/all",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    courseController.getAllCourses(userData)
    .then(result => res.send (result))
})

// Route for retrieving all active courses

router.get("/",(req,res)=>{
    courseController.getAllActive()
    .then(result => res.send(result))
})
// Route for retrieving a specific course

router.get("/:courseId",(req,res)=>{
    console.log(req.params.courseId)
    courseController.getCourse(req.params)
    .then(result => res.send(result))
})

router.put("/:courseId",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        courseController.updateCourse(req.params,req.body)
        .then(result => res.send(result))
    }else{
        res.send({auth:"failed"})
    }
})

router.put("/archive/:courseId",auth.verify,(req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
    courseController.archiveCourse(req.params,req.body)
    .then(result => res.status(200).send(result))
    }else{
        res.send("Unauthorized Personnel")
    }

})
module.exports = router